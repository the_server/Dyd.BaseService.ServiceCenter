﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XXF.BaseService.ServiceCenter.Client
{
    /// <summary>
    /// 客户端Sdk帮助类
    /// </summary>
    public class ClientSDKHelper
    {
        /// <summary>
        /// 生成服务Sdk文件至目录
        /// </summary>
        /// <param name="servicenamespace"></param>
        /// <param name="tofiledir"></param>
        public static void ToLocalFile(string servicenamespace,string tofiledir)
        {
            XXF.BaseService.ServiceCenter.Client.Sdk.SDKProvider sdk = new XXF.BaseService.ServiceCenter.Client.Sdk.SDKProvider();
            sdk.ToLocalFile(servicenamespace, tofiledir);
        }
    }
}
